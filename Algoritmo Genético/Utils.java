import java.lang.reflect.Array;
import java.util.ArrayList;

public class Utils {
	public ArrayList<Cidade> copyArrayCidades(ArrayList<Cidade> cidades) {
		ArrayList<Cidade> copia = new ArrayList<Cidade>();
		for (Cidade cidade : cidades) {
			ArrayList<Double> coordenadas = new ArrayList<Double>();
			coordenadas.add(cidade.getX());
			coordenadas.add(cidade.getY());

			Cidade cidadeCopy = new Cidade(coordenadas, cidade.getNome());

			copia.add(cidadeCopy);
		}
		return copia;
	}

	public Cidade encontraVizinhoMaisProximo(Cidade cidade, ArrayList<Cidade> listaDeCidades) {
		Cidade vizinhoMaisProximo = null;
		double melhorDistancia = 0;
		int i = 0;

		while (i <= listaDeCidades.size() - 1) {
			Cidade possivelVizinhoMaisProximo = listaDeCidades.get(i);
			double distanciaEntreCidades = cidade.distanciaParaCidade(possivelVizinhoMaisProximo);
			if (distanciaEntreCidades < melhorDistancia || melhorDistancia == 0) {
				melhorDistancia = distanciaEntreCidades;
				vizinhoMaisProximo = possivelVizinhoMaisProximo;
			}

			i++;
		}
		return vizinhoMaisProximo;
	}

	public double calculaFitness(ArrayList<Cidade> itinerario) {
		double fitness = 0;
		Cidade cidadeAtual;
		Cidade proximaCidade;
		for (int i = 0; i < itinerario.size(); i++) {
			cidadeAtual = itinerario.get(i);

			if (i < itinerario.size() - 1) {
				proximaCidade = itinerario.get(i + 1);
			} else {
				proximaCidade = itinerario.get(0);
			}

			fitness += cidadeAtual.distanciaParaCidade(proximaCidade);
		}
		return fitness;
	}

	public void traduzPopulacaoParaTeste(String cidades) {

		String[] resultado = cidades.split(",");

		for (int i = 0; i < resultado.length; i++) {
			int numero = Integer.parseInt(resultado[i]);
			
			switch (numero) {
			case 0:
				System.out.print("A,");
				break;
			case 1:
				System.out.print("B,");
				break;

			case 2:
				System.out.print("C,");
				break;
			case 3:
				System.out.print("D,");
				break;
			case 4:
				System.out.print("E,");
				break;
			case 5:
				System.out.print("F,");
				break;
			case 6:
				System.out.print("G,");
				break;
			case 7:
				System.out.print("H,");
				break;
			case 8:
				System.out.print("I,");
				break;
			case 9:
				System.out.print("J,");
				break;
			case 10:
				System.out.print("K,");
				break;
			}
		}
	}
}
