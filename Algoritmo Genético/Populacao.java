import java.util.ArrayList;

public class Populacao {
	private ArrayList<Individuo> individuos = new ArrayList<Individuo>();

	public Populacao(ArrayList<Cidade> cidades, int qtdCidades) {
		int i = 0;
		do {
			Individuo individuo = new Individuo(cidades);
			if (getIndividuos().size() == 0 || !detectaClone(individuo)) {
				setIndividuo(individuo);
				i++;
			}
		} while (i != qtdCidades);
	}

	public boolean detectaClone(Individuo individuo) {
		boolean clone = true;
		for (int i = 0; i < getIndividuos().size(); i++) {
			clone = true;
			for (int j = 0; j < getIndividuo(i).getItinerario().size(); j++) {
				if (getIndividuo(i).getItinerario().get(j).getNome() != individuo.getItinerario().get(j).getNome()) {
					clone = false;
					break;
				}
			}
			if (clone) {
				return true;
			}
		}
		return clone;
	}

	public Individuo getIndividuo(int i) {
		return individuos.get(i);
	}

	public ArrayList<Individuo> getIndividuos() {
		return individuos;
	}

	public void setIndividuo(Individuo individuo) {
		individuos.add(individuo);
	}

	public void print() {
		System.out.println(" --------------- POPULACAO ---------------");
		for (Individuo individuo : individuos) {
			individuo.print(true);
		}
		System.out.println(" -----------------------------------------");
	}

}
