import java.util.ArrayList;

// Operador OX
public class Cruzamento {
	private int tamanhoFaixaDeCorte;
	private ArrayList<Individuo> descendentes = new ArrayList<Individuo>();

	public Cruzamento(ArrayList<Individuo> pais, int tamanhoFaixaDeCorte, long inicioExecucao) {
		this.tamanhoFaixaDeCorte = (int) (pais.get(0).getItinerario().size() * ((double) tamanhoFaixaDeCorte / 100));
		Individuo descendente;
		
		// OBTEM 1� INDIVIDUO
		descendente = realizaCruzamento(pais.get(0), pais.get(1));
		descendente.setTime(inicioExecucao);
		descendentes.add(descendente);

		// OBTEM 2� INDIVIDUO
		descendente =  realizaCruzamento(pais.get(1), pais.get(0));
		descendente.setTime(inicioExecucao);
		descendentes.add(descendente);

	}

	public Individuo realizaCruzamento(Individuo pai, Individuo mae) {
		int posicaoCorte1 = (int) (Math.random() * (pai.getItinerario().size() - tamanhoFaixaDeCorte));
		int posicaoCorte2 = (posicaoCorte1 + tamanhoFaixaDeCorte) - 1;

		ArrayList<Cidade> cidadesPai = pai.getItinerario();
		ArrayList<Cidade> cidadesMae = mae.getItinerario();
		ArrayList<Cidade> cidadesDescendente = new ArrayList<Cidade>();

		for (int i = 0; i <= cidadesPai.size() - 1; i++) {
			cidadesDescendente.add(null);
		}

		for (int i = posicaoCorte1; i <= posicaoCorte2; i++) {
			cidadesDescendente.set(i, cidadesPai.get(i));
		}

		boolean fim = false;
		int l = posicaoCorte1;
		do {

			boolean adiciona = true;

			// PROCURA SE DETERMINADA CIDADE J� EST� PRESENTE NA FAIXA DE CORTE
			for (int i = posicaoCorte1; i <= posicaoCorte2; i++) {
				if (cidadesPai.get(i).getNome() == cidadesMae.get(l).getNome()) {
					adiciona = false;
					break;
				}

			}

			// CASO A CIDADE AINDA N�O ESTEJA INSERIDA NO VETOR cidadesDescendente
			// ESTE C�DIGO PROCURA O PRIMEIRO NULL INSERE NO LUGAR
			if (adiciona) {
				for (int i = 0; i < cidadesDescendente.size(); i++) {
					if (cidadesDescendente.get(i) == null) {
						cidadesDescendente.set(i, cidadesMae.get(l));
						break;
					}
				}
			}

			l++;

			if (l == cidadesDescendente.size()) {
				l = 0;
			}
			if (l == posicaoCorte1) {
				fim = true;
			}
		} while (!fim);

		Individuo descendente = new Individuo();
		descendente.setDescendente(cidadesDescendente);
		return descendente;
		
	}

	public void print() {
		System.out.println("-------------- DESCENDENTES -------------");
		for (Individuo descendente : descendentes) {
			descendente.print(true);
		}
		System.out.println("-----------------------------------------");
	}

	public ArrayList<Individuo> getDescendentes() {
		return this.descendentes;
	}
}
