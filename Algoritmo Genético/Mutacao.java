import java.util.ArrayList;

public class Mutacao {
	public ArrayList<Individuo> descendentes;

	public Mutacao(ArrayList<Individuo> descendentes, int taxaDeMutacao) {
		this.descendentes = descendentes;

		for (int i = 0; i < descendentes.size(); i++) {
			possibilitadeMutacao(descendentes.get(i), taxaDeMutacao);
		}
	}

	public void possibilitadeMutacao(Individuo descendente, int taxaDeMutacao) {

		ArrayList<Integer> possibilidades = new ArrayList<Integer>();
		int i = 0;
		while (i < taxaDeMutacao) {
			int possibilidade = (int) (Math.random() * 100);
			if (!possibilidades.contains(possibilidade)) {
				possibilidades.add(possibilidade);
				i++;
			}
		}

		int possibilidadeDeMutacao = (int) (Math.random() * (100 - taxaDeMutacao));

		if (possibilidades.contains(possibilidadeDeMutacao)) {
			mutacao(descendente);
		}

	}

	// SELECIONA UM INDIVIDUO ALEATORIO E ALTERA SUA POSI��O NO
	// ArrayList<Cidade>cidades
	public void mutacao(Individuo descendente) {
		int posicaoCidadeSelecionada;
		int posicaoCidadeDestino;

		do {
			posicaoCidadeSelecionada = (int) (Math.random() * descendente.getItinerario().size());
			posicaoCidadeDestino = (int) (Math.random() * descendente.getItinerario().size());
		} while (posicaoCidadeSelecionada == posicaoCidadeDestino);

		Cidade cidadeSelecionada = descendente.getItinerario().get(posicaoCidadeSelecionada);
		Cidade cidadeDestino = descendente.getItinerario().get(posicaoCidadeDestino);

		descendente.getItinerario().set(posicaoCidadeDestino, cidadeSelecionada);
		descendente.getItinerario().set(posicaoCidadeSelecionada, cidadeDestino);

		descendente.setMutacao();

	}

	public void print() {
		System.out.println("----------------- MUTACAO ---------------");
		for (Individuo individuo : descendentes) {
			individuo.print(true);
		}
		System.out.println("-----------------------------------------");
	}

	public ArrayList<Individuo> getDescendentes() {
		return this.descendentes;
	}
}
