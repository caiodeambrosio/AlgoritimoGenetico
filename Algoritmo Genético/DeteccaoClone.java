import java.util.ArrayList;

public class DeteccaoClone {
	private ArrayList<Individuo> descendentesPosDeteccaoClone = new ArrayList<Individuo>();
	private ArrayList<Individuo> descendentesPosMutacao = new ArrayList<Individuo>();

	public DeteccaoClone(ArrayList<Individuo> individuosPopulacao, ArrayList<Individuo> descendentesPosMutacao) {
		this.descendentesPosMutacao = descendentesPosMutacao;

		// ADICIONA OS INDIVIDUOS COM FITNESS J� PRESENTE NA POPULACAO NO ARRAY CLONES
		for (Individuo individuo : individuosPopulacao) {
			for (Individuo descendentePosMutacao : descendentesPosMutacao) {
				if (individuo.getFitness() == descendentePosMutacao.getFitness()) {
					descendentePosMutacao.setClone();
				}
			}
		}

		// REMOVE OS CLONES DO ARRAY DESCENDENTESPOSMUTACAO
		for (Individuo descendente : descendentesPosMutacao) {
			if (!descendente.getClone()) {
				descendentesPosDeteccaoClone.add(descendente);
			}
		}

	}

	public ArrayList<Individuo> getDescendentesPosDeteccaoClone() {
		return this.descendentesPosDeteccaoClone;
	}

	public void print() {
		System.out.println("----------- DETEC��O DE CLONES ----------");
		for (Individuo descendente : descendentesPosMutacao) {
			descendente.print(true);
		}
		System.out.println("-----------------------------------------");
	}
}
