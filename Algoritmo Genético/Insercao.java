import java.util.ArrayList;

public class Insercao {
	Individuo melhorFilho = null;
	private Individuo melhorDescendente = new Individuo();
	private Individuo piorPai = new Individuo();
	private Populacao populacao;

	public Insercao(Populacao populacao, ArrayList<Individuo> pais, ArrayList<Individuo> descendentes) {
		this.populacao = populacao;
		if (!descendentes.isEmpty()) {

			// ENCONTRA MELHOR DESCENDENTE
			for (int i = 0; i < descendentes.size(); i++) {
				if ((melhorDescendente.getFitness() == 0)|| (descendentes.get(i).getFitness() < melhorDescendente.getFitness())) {
					melhorDescendente = descendentes.get(i);
				}
			}

			// ENCONTRA PIOR PAI
			for (int i = 0; i < pais.size(); i++) {
				if ((piorPai.getFitness() == 0) || (pais.get(i).getFitness() > piorPai.getFitness())) {
					piorPai = pais.get(i);
				}
			}

			// INSERE FILHO NA POPULA��O CASO O MELHOR FILHO SEJA MELHOR QUE O PIOR PAI
			if(melhorDescendente.getFitness() < piorPai.getFitness()) {
				populacao.getIndividuos().remove(piorPai);
				populacao.getIndividuos().add(melhorDescendente);
			}
		}

	}

	public void print() {
//		System.out.println("---------------- INSER��O ---------------");

//		System.out.print("Melr Des: ");
		melhorDescendente.print(true);

//		System.out.print("Pior Pai: ");
//		piorPai.print();

//		populacao.print();
		System.out.println("-----------------------------------------");
	}

}
