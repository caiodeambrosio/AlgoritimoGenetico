import java.util.ArrayList;

public class MelhorIndividuo extends Individuo{
	Individuo melhorIndividuo;
	
	public MelhorIndividuo(ArrayList<Individuo> individuos) {
		melhorIndividuo = individuos.get(individuos.size()-1);
		for (Individuo individuo : individuos) {
			if(individuo.getFitness() < melhorIndividuo.getFitness()) {
				melhorIndividuo = individuo;
			}
		}
	}
	
	public Individuo getMelhorIndividuo() {
		return this.melhorIndividuo;
	}
}
