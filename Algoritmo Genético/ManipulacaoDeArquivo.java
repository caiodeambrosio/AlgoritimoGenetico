import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class ManipulacaoDeArquivo {
	String local = "BancoDeDados.txt";

	public ManipulacaoDeArquivo() {
		try {
			limparArquivo();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void salvarIndividuo(Individuo individuo) throws IOException {
		salvar(individuo.print(false));
	}

	public void salvar(String dado) throws IOException {

		try {
			FileWriter fw = new FileWriter(local, true);
			BufferedWriter conexao = new BufferedWriter(fw);
			conexao.write(dado);
			conexao.newLine();
			conexao.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void limparArquivo() throws IOException {
		FileWriter fw = new FileWriter(local);
		BufferedWriter conexao = new BufferedWriter(fw);
		fw.write("");
		conexao.close();

	}
}
