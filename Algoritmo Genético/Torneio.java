import java.util.ArrayList;

public class Torneio {
	private ArrayList<Individuo> concorrentes = new ArrayList<Individuo>();
	private ArrayList<Individuo> vencedores = new ArrayList<Individuo>();

	public Torneio(ArrayList<Individuo> individuosPopulacao, int qtdConcorrentes) {
		for (int i = 0; i < 2; i++) {
			novoTorneio(individuosPopulacao, qtdConcorrentes);
		}

	}

	public void novoTorneio(ArrayList<Individuo> individuosPopulacao, int qtdConcorrentes) {
		// CAPTURA INDIVIDUOS ALEATORIOS E ENCONTRA O MENOR ENTRE ELES
		Individuo vencedor = new Individuo();
		for (int i = 0; i < qtdConcorrentes; i++) {
			Individuo individuo = individuosPopulacao.get((int) (Math.random() * individuosPopulacao.size()));

			if ((vencedor.getFitness() == 0) || (individuo.getFitness() < vencedor.getFitness())) {
				vencedor = individuo;
			}
		}


		for (Individuo individuo : vencedores) {
			if (individuo.getFitness() == vencedor.getFitness()) {
				novoTorneio(individuosPopulacao, qtdConcorrentes);
				return;
			}

		}
		vencedores.add(vencedor);
	}

	public ArrayList<Individuo> getVencedores() {
		return this.vencedores;
	}

	public void print() {
		System.out.println(" ----------- PAIS SELECIONADOS -----------");
		for (Individuo individuo : vencedores) {
			individuo.print(true);
		}
		System.out.println(" -----------------------------------------");
	}

}
