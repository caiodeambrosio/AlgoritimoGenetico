import java.util.ArrayList;

public class Cidade {
	private int nome;

	private double x;
	private double y;

	public Cidade() {
	}

	public Cidade(ArrayList<Double> coordenadas, int index) {
		setNome(index);
		this.x = coordenadas.get(0);
		this.y = coordenadas.get(1);
	}
	
	public double distanciaParaCidade(Cidade cidade) {
		double x = Math.abs(getX() - cidade.getX());
		double y = Math.abs(getY() - cidade.getY());
		return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
	}

	public double getX() {
		return this.x;
	}

	public double getY() {
		return this.y;
	}

	public int getNome() {
		return this.nome;
	}

	public void setNome(int nome) {

		this.nome = nome;
	}

	public void print() {
		String vizinhoAnteriorStr = "";
		System.out.println("Coord: X - " + getX() + "\t Y - " + getY() + vizinhoAnteriorStr);
	}

}