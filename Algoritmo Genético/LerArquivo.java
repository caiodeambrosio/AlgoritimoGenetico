
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class LerArquivo {

	public ArrayList<Cidade> LerArquivo(String arquivo) {
		String path = "Arquivos/Problemas/" + arquivo;
		String line;
		String[] lineArray;
		ArrayList<Double> coordenadas = null;
		ArrayList<Cidade> cidades = new ArrayList<Cidade>();
		Cidade cidade;
		
		try {
			BufferedReader arq = new BufferedReader(new FileReader(path));

			while (true) {
				line = arq.readLine();
				
				// A String "*START*" dever� ser inserida no arquivo de coodenadas imediatamente
				// a cima da primeira coordenada, desta forma o algoritmo encontrar� o inicio das coordenadas
				if (line.contains("*START*")) {
					int i = 0;
					while (true) {
						line = arq.readLine();
						coordenadas = new ArrayList<Double>();
						if (line.equals("EOF")) {
							break;
						}

						lineArray = line.split(" ");

						for (int j = 0; j < lineArray.length; j++) {
							if (j != 0 && !lineArray[j].equals("")) {
								coordenadas.add(Double.parseDouble(lineArray[j]));
							}
						}
						
						cidade = new Cidade(coordenadas, i);
						cidades.add(cidade);
						i++;
					}
					break;
				}

			}
			arq.close();
		} catch (Exception e) {
			System.err.println("Erro com o arquivo: " + e.getMessage() + ".\n");
		}
		Collections.shuffle(cidades);
		return cidades;
	}
}
