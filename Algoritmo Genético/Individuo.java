import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Individuo {
	private static Utils utils = new Utils();
	private ArrayList<Cidade> itinerario = new ArrayList<>();
	private double fitness = 0;
	private boolean clone = false;
	private boolean mutacao = false;
	private long time = 0;

	public Individuo() {
	}

	public void setDescendente(ArrayList<Cidade> cidades) {
		setItinerario(cidades);
		setFitness(utils.calculaFitness(cidades));
	}

	public Individuo(ArrayList<Cidade> cidades) {
		ArrayList<Cidade> listaDeCidades = utils.copyArrayCidades(cidades);

		Cidade cidadeOrigem = listaDeCidades.get((int) (Math.random() * listaDeCidades.size()));

		// FORMA O arrayList<Cidade>cidades ORDENADO PELAS MENORES DISTANCIAS
		int i = 0;
		while (!listaDeCidades.isEmpty()) {
			addCidadeItinerario(cidadeOrigem);
			listaDeCidades.remove(cidadeOrigem);
			cidadeOrigem = utils.encontraVizinhoMaisProximo(cidadeOrigem, listaDeCidades);
		}
		setFitness(utils.calculaFitness(getItinerario()));
	}

	public double getFitness() {
		return this.fitness;
	}

	public void setFitness(double fitness) {
		this.fitness = fitness;
	}

	public ArrayList<Cidade> getItinerario() {
		return this.itinerario;
	}

	public void setItinerario(ArrayList<Cidade> cidades) {
		this.itinerario = cidades;
	}

	public void addCidadeItinerario(Cidade cidade) {
		this.itinerario.add(cidade);
	}

	public boolean getMutacao() {
		return this.mutacao;
	}

	public void setMutacao() {
		setFitness(utils.calculaFitness(getItinerario()));
		this.mutacao = true;
	}

	public boolean getClone() {
		return clone;
	}

	public void setClone() {
		this.clone = true;
	}

	public String print(boolean exibir) {
		String string = "[";
		for (int i = 0; i < itinerario.size(); i++) {
			int valor = itinerario.get(i).getNome();
			if (valor < 100 && valor >= 10) {
				string += "0";
			} else if (valor < 10) {
				string += "00";
			}
			string += valor + ",";			
		}
		if (getItinerario().get(0).getNome() < 100 && getItinerario().get(0).getNome() >= 10) {
			string += "0";
		} else if (getItinerario().get(0).getNome() < 10) {
			string += "00";
		}
		string += getItinerario().get(0).getNome();
		
		string += "] - \tFitness: " + getFitness() + " \tTempo : " + getTempoFormatado();

		if (getClone()) {
			string +=" CLONE!!";
		}

		if (getMutacao()) {
			string +=" MUTA��O!!";
		}
		
		if (exibir) {
			System.out.println(string);
		}
		return string;
	}

	public void setTime(long inicioExecucao) {
		this.time = System.currentTimeMillis() - inicioExecucao;
	}

	public long getTime() {
		return this.time;
	}

	public String getTempoFormatado() {
		String tempo = new SimpleDateFormat("mm:ss.SSS").format(new Date((this.time)));
		return tempo;
	}

}
