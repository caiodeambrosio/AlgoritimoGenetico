import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.Popup;

public class Principal {

	private static boolean start = true;

	public static void main(String[] args) throws IOException {
		Individuo melhorIndividuo = new Individuo();
		Geracao geracao = new Geracao();
		ManipulacaoDeArquivo manipulacaoDeArquivo = new ManipulacaoDeArquivo();

		
		// VARI�VEIS DE CONTROLE
		int qtdCidadesPopulacao = 10;
		int qtdConcorrentesTorneio = 3;
		int tamanhoFaixaCorte = 70; // 70% de Semelhan�a com os pais
		int taxaDeMutacao = 1; // 1% de Chance de um individuo sofrer muta��o

		// L� ARQUIVO DE COORDENADAS E CRIA UM ARRAYLIST DE CIDADES
		LerArquivo arquivo = new LerArquivo();
		ArrayList<Cidade> cidades = arquivo.LerArquivo("ali10.tsp");


		// POPULACAO
		Populacao populacao = new Populacao(cidades, qtdCidadesPopulacao);
		ArrayList<Individuo> individuosPopulacao = populacao.getIndividuos();

		// INICIALIZA CONTAGEM DO TEMPO
		long inicioExecucao = System.currentTimeMillis();
		long tempoDeExecucao = 3600;
		Timer timer = new Timer();

		timer.schedule(new TimerTask() {
			public void run() {
				start = false;
			}
		}, tempoDeExecucao * 1000);
		do {
			// TORNEIO
			Torneio torneio = new Torneio(individuosPopulacao, qtdConcorrentesTorneio);
			ArrayList<Individuo> pais = torneio.getVencedores();
			// torneio.print();

			// CRUZAMENTO
			Cruzamento cruzamento = new Cruzamento(pais, tamanhoFaixaCorte, inicioExecucao);
			ArrayList<Individuo> descendentes = cruzamento.getDescendentes();
			// cruzamento.print();

			// MUTA��O
			Mutacao mutacao = new Mutacao(descendentes, taxaDeMutacao);
			ArrayList<Individuo> descendentesPosMutacao = mutacao.getDescendentes();
			// mutacao.print();

			// DETEC��O DE CLONES
			DeteccaoClone deteccaoClone = new DeteccaoClone(individuosPopulacao, descendentesPosMutacao);
			ArrayList<Individuo> descendentesPosDeteccaoClone = deteccaoClone.getDescendentesPosDeteccaoClone();
			// deteccaoClone.print();

			// INSER��O
			Insercao insercao = new Insercao(populacao, pais, descendentesPosDeteccaoClone);
			// insercao.print();

			// SELECIONA E ARMAZENA MELHOR INDIVIDUO DA POPULACAO
			for (Individuo individuoPopulacao : individuosPopulacao) {
				if (melhorIndividuo.getFitness() == 0 || individuoPopulacao.getFitness() < melhorIndividuo.getFitness()) {
					melhorIndividuo = individuoPopulacao;
					melhorIndividuo.print(true);
					manipulacaoDeArquivo.salvarIndividuo(melhorIndividuo);
					
				}
			}

		} while (start);


		timer.cancel();
	}
}